# Zero (PiS, SoSe 2021)

Autor: Khaled Abuhaltam, 5275157

<!-- TOC depthfrom:2 -->

- [Kurzbeschreibung](#kurzbeschreibung)
- [Screenshot](#screenshot)  
- [Bedienungshinweise](#bedienungshinweise)
- [Übersicht](#datei%C3%BCbersicht-und-lines-of-code)
- [Quellen](#verwendete-quellen)

<!-- /TOC -->

## Kurzbeschreibung


In diesem Mini-Spiel steuerst du eine Kanone, mit der du einen neuen Highscore erreichen kannst. Um das Spiel zu meistern, musst du mit der Maus die Kanone so steuern, dass die Projektile die Ziele treffen.<br> 

Je höher die Schwierigkeitsstufe, desto kleiner sind deine Projektile und die Ziele.<br>
Mit der Zeit erscheinen mehr Gegner, aber dadurch schießt die Waffe auch schneller.

Sobald einer deiner gegner den unteren Bildschirmrand erreicht hat, ist das Spiel zu Ende.<br>
Der Score deines Versuchs sowie der Highscore werden dir oben angezeigt. (66 Wörter)

## Screenshot

![Screenshot](app/src/main/resources/Screenshot.png)


## Bedienungshinweise

- Um das Spiel zu starten, drücke auf die Schaltfläche `start game`.
- Um eine andere Schwierigkeit auszuwählen, drücke auf `select difficulty`.<br>
  Anschließend wählst du eine der drei Schwierigkeiten aus: `easy`, `medium`, `hard`<br>
  Die ausgewählte Schwierigkeit ist in einem hellblauen Hintergrund hinterlegt.
- Um das Programm zu beenden drücke die Taste `esc` oder wähle mit der Maus `exit`.
- Sobald du das Spiel gestartet hast, steuerst du die Kanone mit dem Mauszeiger.<br> 
  Du kannst die Kanone nur Horizontal auf dem Bildschirm bewegen.
- Sobald einer der Gegner den unteren Bildschirmrand erreicht hat, kannst du das Spiel mit `restart` neustarten.
- Falls du die Schwierigkeit ändern möchtest, drückst du auf `change difficulty`.

## Dateiübersicht und Lines of Code

    \README.md
    \app\build.gradle
    \app\core.jar
    \app\highscore.txt
    \app\src\main\java\Project_Zero\App.java
    \app\src\main\java\Project_Zero\Bullet.java
    \app\src\main\java\Project_Zero\Button.java
    \app\src\main\java\Project_Zero\Difficulty.java
    \app\src\main\java\Project_Zero\Draw.java
    \app\src\main\java\Project_Zero\Engine.java
    \app\src\main\java\Project_Zero\GameEngine.java
    \app\src\main\java\Project_Zero\Gamestate.java
    \app\src\main\java\Project_Zero\Gun.java
    \app\src\main\java\Project_Zero\Item.java
    \app\src\main\resources\aim.png
    \app\src\main\resources\arrow.png
    \app\src\main\resources\bullet.png
    \app\src\main\resources\gun.png
    \app\src\main\resources\item.png
    \app\src\main\resources\Screenshot.png
    \app\src\test\java\Project_Zero\AppTest.java

    -------------------------------------------------------------------------------
    Language                     files          blank        comment           code
    -------------------------------------------------------------------------------
    XML                             15              0              0           1914
    Java                            11            150            167            534
    HTML                             3              0              0            362
    CSS                              2             49              0            214
    JavaScript                       1             45              1            148
    Bourne Shell                     1             23             36            126
    Markdown                         1             18              0             71
    DOS Batch                        1             21              2             66
    Gradle                           2              9             21             17
    JSON                             1              0              0             11
    -------------------------------------------------------------------------------
    SUM:                            38            315            227           3463
    -------------------------------------------------------------------------------

## Verwendete Quellen

- https://jeffreythompson.org/collision-detection/table_of_contents.php
- https://processing.org/reference
- https://toppng.com
- http://shuttershock.com
- https://www.pngitem.com/