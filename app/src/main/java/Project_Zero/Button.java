package Project_Zero;

import processing.core.PGraphics;

import static processing.core.PConstants.CENTER;

public class Button {

    private final float x, y, w, h;
    private final String text;
    private static float mouseX, mouseY;
    private static boolean mouseClicked;
    private boolean onClick = false;
    private boolean selected = false;

    public Button(String text, float x, float y, float w, float h) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = w;
    }

    public static void setMouseXY(float mouseX, float mouseY) {
        Button.mouseX = mouseX;
        Button.mouseY = mouseY;
    }

    public static void mouseClicked(boolean mouseClicked){
        Button.mouseClicked = mouseClicked;
    }

    public float getY() {
        return y;
    }

    public float getH() {
        return h;
    }

    public float getX() {
        return x;
    }

    public float getW() {
        return w;
    }

    public boolean onClick(){
        return onClick;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    /**
     * Check if Mouse is hoovering above the Button
     * @return
     */
    public boolean mouseIsOver() {
        return mouseX > x && mouseX < (x + w) && mouseY > y && mouseY < (y + h);
    }

    public void draw(PGraphics g) {
        // Set onClick
        if (mouseClicked && mouseIsOver()) onClick = true;
        else onClick = false;

        // Switch color if mouse is hoovering
        if (!mouseIsOver()) {
            if (!selected){
                g.fill(110);
                g.stroke(110);
            }else{
                g.fill(0,191,255);
                g.stroke(0,191,255);
            }
        }
        else {
            if (!selected){
                g.fill(80);
                g.stroke(80);
            }else{
                g.fill(30,144,255);
                g.stroke(30,144,255);
            }

        }
        g.rect(x, y, w, h, 10);

        // Write text on button
        g.textAlign(CENTER, CENTER);
        g.fill(255);
        g.textSize(h / 2);
        g.text(text, x + (w / 2), y + (h / 2));

    }
}
