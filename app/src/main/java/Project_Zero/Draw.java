package Project_Zero;

import org.jetbrains.annotations.*;
import processing.core.PApplet;
import processing.core.PImage;

import static Project_Zero.Difficulty.*;
import static Project_Zero.Gamestate.*;

public class Draw extends PApplet {

    // Objects
    GameEngine gameEngine;
    Button buttonStart, buttonDifficulty, buttonExit, buttonReStart, buttonChangeDifficulty, buttonEasy, buttonMedium, buttonHard;
    Gun gun;

    // Images
    PImage gunImage, bulletImage, itemImage, cursorImage;

    // Variables
    float buttonX, buttonY, buttonW, buttonH, bulletSpeed, itemSpeed;
    int imageRadius, score;

    /**
     * Button will be selected and the others will be unselected
     *
     * @param button hand overed wich will be selected
     */
    public void selectButton(@NotNull Button button) {
        buttonEasy.setSelected(button.equals(buttonEasy));
        buttonMedium.setSelected(button.equals(buttonMedium));
        buttonHard.setSelected(button.equals(buttonHard));
    }

    /**
     * define the settings() from processing
     */
    public void settings() {
        size(displayHeight / 2, displayHeight - 200);
    }

    public void setup() {
        // Surface
        surface.setTitle("Zero");
        surface.setLocation((displayWidth / 2) - (width / 2), 20);

        // GameEngine
        gameEngine = new GameEngine();
        gameEngine.setBulletTime(millis());
        gameEngine.setItemTime(millis());
        // Load highscore from highscore.txt
        gameEngine.rows = loadStrings("highscore.txt");

        // Images
        imageMode(CENTER);
        gunImage = loadImage("gun.png");
        gunImage.resize(width / 8, width / 8);
        bulletImage = loadImage("bullet.png");
        itemImage = loadImage("item.png");
        cursorImage = loadImage("aim.png");

        // Methods
        buttonW = (float) width / 3;
        buttonH = buttonW / 4;
        buttonX = (width >> 1) - (buttonW / 2);
        buttonY = (height >> 1) - (buttonH / 2);

        // Initialize main buttons
        buttonStart = new Button("start game", buttonX, (float) height / 3, buttonW, buttonH);
        buttonDifficulty = new Button("select difficulty", buttonX, buttonStart.getY() + buttonStart.getH() + 10, buttonW, buttonH);
        buttonExit = new Button("exit", buttonX, buttonDifficulty.getY() + buttonDifficulty.getH() + 10, buttonW, buttonH);
        // Initialize gameover buttons
        buttonReStart = new Button("restart", buttonX, (float) height / 3, buttonW, buttonH);
        buttonChangeDifficulty = new Button("change difficulty", buttonX, buttonReStart.getY() + buttonReStart.getH() + 10, buttonW, buttonH);
        // Initialize difficulty buttons
        buttonEasy = new Button("easy", buttonStart.getX() + buttonStart.getW() + 10, buttonStart.getY(), buttonH * 2, buttonH);
        buttonMedium = new Button("medium", buttonStart.getX() + buttonStart.getW() + 10, buttonDifficulty.getY(), buttonH * 2, buttonH);
        buttonHard = new Button("hard", buttonStart.getX() + buttonStart.getW() + 10, buttonExit.getY(), buttonH * 2, buttonH);

        // Initialize gun
        gun = new Gun(gunImage, (width >> 1) - (gunImage.width >> 1), (float) height - (gunImage.height >> 1));

        // Initialize speed of moving objects
        itemSpeed = (float) width / 100;
        bulletSpeed = (float) width / 25;

        // Change cursor image (not working properly on mac!)
        surface.setCursor(cursorImage, width / 100, width / 100);
    }

    public void draw() {
        // Background default
        background(100);

        // Change necessary things in case of all difficulties
        switch (gameEngine.getDifficulty()) {
            case EASY -> {
                background(130, 250, 88);
                selectButton(buttonEasy);
                imageRadius = width / 45;
            }
            case MEDIUM -> {
                background(220, 170, 26);
                selectButton(buttonMedium);
                imageRadius = width / 55;
            }
            case HARD -> {
                background(140, 48, 48);
                selectButton(buttonHard);
                imageRadius = width / 65;
            }
            default -> throw new IllegalStateException("Unexpected value: " + gameEngine.getDifficulty());
        }

        // MouseXY for button
        Button.setMouseXY(mouseX, mouseY);
        Button.mouseClicked(mousePressed);

        // Save Highscore
        gameEngine.setHighscore(Integer.parseInt(gameEngine.rows[0]));
        saveStrings("highscore.txt", gameEngine.rows);

        // Draw score and highscore
        text("score: " + score, 100, 50);
        text("highscore: " + gameEngine.getHighscore(), width - 200, 50);

        // Switch between gamemodes
        switch (gameEngine.getGamestate()) {

            // MAINMENU
            case MAINMENU -> {

                // Show buttons
                buttonStart.draw(g);
                buttonDifficulty.draw(g);
                buttonExit.draw(g);

                // Check if buttons pressed
                if (buttonStart.onClick()) {
                    gameEngine.setGamestate(INGAME);
                    buttonDifficulty.setSelected(false);
                }
                if (buttonDifficulty.onClick()) buttonDifficulty.setSelected(true);

                if (buttonDifficulty.isSelected()) {
                    buttonEasy.draw(g);
                    buttonMedium.draw(g);
                    buttonHard.draw(g);

                    // Select difficulty
                    if (buttonEasy.onClick()) {
                        gameEngine.setDifficulty(EASY);
                        buttonDifficulty.setSelected(false);
                    }
                    if (buttonMedium.onClick()) {
                        gameEngine.setDifficulty(MEDIUM);
                        buttonDifficulty.setSelected(false);
                    }
                    if (buttonHard.onClick()) {
                        gameEngine.setDifficulty(HARD);
                        buttonDifficulty.setSelected(false);
                    }
                }

                // Exit
                if (buttonExit.onClick()) {
                    exit();
                }
            }

            // INGAME
            case INGAME -> {

                score = gameEngine.getScore();

                // Draw gun
                gun.setMouseX(mouseX);
                gun.draw(g);

                // Check time to spawn new item
                if (gameEngine.checkTime(gameEngine.getItemSpawnTime(), gameEngine.getItemTime(), millis())) {
                    gameEngine.items.add(new Item(itemImage, gameEngine.randomSpawn(width), 0, itemSpeed, imageRadius * 2));
                    gameEngine.setItemTime(millis());
                }
                // Check time to spawn new bullet
                if (gameEngine.checkTime(gameEngine.getBulletSpawnTime(), gameEngine.getBulletTime(), millis())) {
                    gameEngine.bullets.add(new Bullet(bulletImage, gun.getX() - 5, gun.getY() - 10, bulletSpeed, imageRadius));
                    gameEngine.setBulletTime(millis());
                }
                // Draw bullets and items
                gameEngine.bullets.forEach(bullet -> bullet.draw(g));
                gameEngine.items.forEach(item -> item.draw(g));

                // CollisionDetection
                gameEngine.collisionDetection(height);
            }

            // GAMEOVER
            case GAMEOVER -> {

                //endbildschirm
                buttonReStart.draw(g);
                buttonChangeDifficulty.draw(g);
                buttonExit.draw(g);

                // Restart
                if (buttonReStart.onClick()) {
                    gameEngine.setGamestate(INGAME);
                }
                // Mainmenu
                if (buttonChangeDifficulty.onClick()) {
                    gameEngine.setGamestate(MAINMENU);
                }
                // Exit
                if (buttonExit.onClick()) {
                    exit();
                }
            }
            default -> throw new IllegalStateException("Unexpected value: " + gameEngine.getGamestate());
        }
    }

    public static void main(String[] args) {
        PApplet.runSketch(new String[]{"test"}, new Draw());
    }
}
