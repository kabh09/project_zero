package Project_Zero;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

import static Project_Zero.Difficulty.*;
import static Project_Zero.Gamestate.*;

public class GameEngine implements Engine {

    //variables
    private int score = 0, highscore = 0;
    private int bulletSpawnTime = 1000, itemSpawnTime = 1200;
    private int bulletTime, itemTime;

    private Gamestate gamestate = MAINMENU;
    private Difficulty difficulty = EASY;

    protected String[] rows = {"0"};
    protected ArrayList<Bullet> bullets = new ArrayList<>();
    protected ArrayList<Item> items = new ArrayList<>();

    /**
     *
     * @return Gamestate
     */
    @Override
    public Gamestate getGamestate() {
        return gamestate;
    }

    /**
     * @param gamestate set gamestate
     */
    @Override
    public void setGamestate(@NotNull Gamestate gamestate) {
        this.gamestate = gamestate;
    }

    /**
     *
     * @return Difficulty
     */
    @Override
    public Difficulty getDifficulty(){
        return difficulty;
    }

    /**
     *
     * @param difficulty set difficulty
     */
    @Override
    public void setDifficulty(@NotNull Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    /**
     *
     * @return int
     */
    @Override
    public int getHighscore() {
        return highscore;
    }

    /**
     *
     * @param highscore set highscore
     */
    @Override
    public void setHighscore(int highscore){
        this.highscore = highscore;
    }

    /**
     *
     * @return int
     */
    @Override
    public int getScore() {
        return score;
    }

    /**
     *
     * @param score set score
     */
    @Override
    public void setScore(int score){
        this.score = score;
        if (this.score > highscore){
            this.highscore = score;
            rows[0] = String.valueOf(score);
        }
        if (bulletSpawnTime > 200) bulletSpawnTime = bulletSpawnTime-6;
        if (itemSpawnTime > 300) itemSpawnTime = itemSpawnTime -5;
    }

    /**
     * Reset the game
     */
    @Override
    public void reset(){
        this.score = 0;
        this.bullets.clear();
        this.items.clear();
        this.gamestate = GAMEOVER;
        this.bulletSpawnTime = 1000;
        this.itemSpawnTime = 1200;
    }

    /**
     *
     * @return bulletSpawnTime
     */
    public int getBulletSpawnTime(){
        return bulletSpawnTime;
    }

    // For test
    public void setBulletSpawnTime(int bulletSpawnTime){
        this.bulletSpawnTime = bulletSpawnTime;
    }


    /**
     *
     * @return itemSpawnTime
     */
    public int getItemSpawnTime(){
        return itemSpawnTime;
    }

    // For test
    public void setItemSpawnTime(int itemSpawnTime){
        this.itemSpawnTime = itemSpawnTime;
    }


    /**
     *
     * @param width of the window
     * @return float
     */
    public float randomSpawn(float width){
        Random random = new Random();
        return random.nextInt((int) width-20) +10 ;
    }


    public int getBulletTime() {
        return bulletTime;
    }

    public void setBulletTime(int bulletTime) {
        this.bulletTime = bulletTime;
    }

    public int getItemTime() {
        return itemTime;
    }

    public void setItemTime(int itemTime) {
        this.itemTime = itemTime;
    }

    /**
     * Check if the temp variable + the time to wait are smaller then the past millis
     * @param time delay
     * @param temp variable
     * @param millis live milliseconds of the program
     * @return if its time to spawn
     */
    public boolean checkTime(int time, int temp, int millis){
        return temp + time <= millis;
    }

    /**
     * Collision detection of bullets with items
     * and bullets with the top border
     * and items with the bottom border
     */
    public void collisionDetection(float height) {
        float distX;
        float distY;
        float distance;

        for (int i = 0; i < items.size(); i++) {
            for (Bullet bullet : bullets) {

                //variables
                distX = bullet.getX() - items.get(i).getX();
                distY = bullet.getY() - items.get(i).getY();
                distance = (float) Math.sqrt((distX * distX) + (distY * distY));

                // Collision detection of bullets with items
                if (distance <= items.get(i).getWidth()) {
                    items.remove(i);
                    setScore(getScore() + 1);
                    rows[0] = String.valueOf(getHighscore());
                    break;
                }
            }
        }
        // Collision detection of bullets with the top border
        IntStream.range(0, bullets.size()).filter(i -> bullets.get(i).getY() <= -50).findFirst().ifPresent(i -> bullets.remove(i));

        // Collision detection of items with the bottom border
        if (items.stream().anyMatch(item -> item.getY() + item.getHeight() >= height)) reset();
    }

    /**
     *
     * @return a String
     */
    public String toString() {
        return "Das ist die toString Methode";
    }
}
