package Project_Zero;

import processing.core.PGraphics;
import processing.core.PImage;

public class Gun {

    float x,y;
    PImage gunImage;

    public Gun(PImage gunImage, float gunX, float gunY){
        this.gunImage = gunImage;
        this.x = gunX;
        this.y = gunY;
    }

    public void setMouseX(float mouseX){
        this.x = mouseX;
    }

    public float getX(){
        return x;
    }

    public float getY(){
        return y;
    }

    public void draw(PGraphics g){
        g.pushMatrix();
        g.translate(x, y);
        g.rotate((float) Math.toRadians(288));
        g.image(gunImage, 0, 0);
        g.popMatrix();

    }
}