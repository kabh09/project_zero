package Project_Zero;

import org.jetbrains.annotations.NotNull;

public interface Engine {

    // Get gamestate
    Gamestate getGamestate();

    // Set gamestate
    void setGamestate(@NotNull Gamestate gamestate);

    // Get difficulty
    Difficulty getDifficulty();

    // Set difficulty
    void setDifficulty(@NotNull Difficulty difficulty);

    // Get highscore
    int getHighscore();

    // Set Highscore
    void setHighscore(int highscore);

    // Get Score
    int getScore();

    // Set Score
    void setScore(int score);

    // Reset the Game and stats
    void reset();


}
