package Project_Zero;
import processing.core.PGraphics;
import processing.core.PImage;

public class Item {

    private  float x, y, speed;
    private PImage itemImage = new PImage();

    // For Test
    private int temp;

    public Item(PImage itemImage, float x, float y, float speed, int itemRadius) {
        itemImage.resize(itemRadius, itemRadius);
        this.itemImage = itemImage;
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    // For test
    public Item(float x, float y, int itemRadius) {
        temp = itemRadius;
        this.x = x;
        this.y = y;
    }

    public float getWidth(){
        // For test
        temp = itemImage.width;
        return temp;
    }

    public float getHeight(){
        return itemImage.height;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    // For test
    public void setY(float y){
        this.y = y;
    }

    public void draw(PGraphics g){
        g.image(itemImage, x, y);
        y += speed;
    }

}
