package Project_Zero;

import processing.core.PGraphics;
import processing.core.PImage;

public class Bullet {

    private float x, y, speed;
    private PImage bulletImage;

    public Bullet(PImage bulletImage, float bulletX, float bulletY, float speed, int bulletRadius) {
        bulletImage.resize(bulletRadius+20, bulletRadius+20);
        this.bulletImage = bulletImage;
        this.x = bulletX;
        this.y = bulletY;
        this.speed = speed;
    }

    // For test
    public Bullet(float bulletX, float bulletY) {
        this.x = bulletX;
        this.y = bulletY;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    // For test
    public void setY(float y){
        this.y = y;
    }

    public void draw(PGraphics g) {
        g.image(bulletImage, x, y);
        y -= speed;
    }
}